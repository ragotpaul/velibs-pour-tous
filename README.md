# Velib's pour tous

## Contributeurs :

- Galleron Erwan
- Phan Christophe
- Ragot Paul

## Dépendance

Soit :

- Java
- PostgreSQL

Soit :

- Docker
- Docker compose (Optionnel)

## Base de donnée

- database : 'velib'
- username : 'velib'
- password : 'bilev'

## Compilation & Lancement

Sans docker (avec le service PostgreSQL déja lancé et configué) :
- Build : `./gradlew clean build`
- Launch : `java -jar build/libs/velib.jar`

Avec docker :
- One command : `./gradlew runPostgreSQL clean buildDocker runDocker`

Avec docker (avec le service PostgreSQL déja lancé et configué) :
- One command : `./gradlew clean buildDocker runDocker`

Pour stopper les containers Docker :
- One command : `./gradlew removePostgreSQL removeDocker`

Pour stopper le container Docker (avec le service PostgreSQL déja lancé et configué) :
- One command : `./gradlew removeDocker`

## Contact

Paul Ragot <ragotpaul@eisti.eu>