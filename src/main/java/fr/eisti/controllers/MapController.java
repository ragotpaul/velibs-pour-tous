package fr.eisti.controllers;

import fr.eisti.api.GoogleMapsAPI;
import fr.eisti.api.JCDecauxAPI;
import fr.eisti.models.Contract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by ragotpaul on 1/15/17.
 */

@Controller
public class MapController {

    @Autowired
    private GoogleMapsAPI googleMapsAPI;

    @Autowired
    private JCDecauxAPI jcDecauxAPI;

    @RequestMapping("/map")
    public ModelAndView map(HttpServletRequest request) throws IOException {
        ModelAndView view = new ModelAndView("index");
        HttpSession session = request.getSession();
        String name;
        if (session.getAttribute("contract") == null) {
            name = "Paris";
        } else {
            Contract contract = (Contract) session.getAttribute("contract");
            name = contract.getName();
        }
        view.addObject("stations", jcDecauxAPI.getAllStations(name));
        view.addObject("initUrl", googleMapsAPI.getUrlInit());
        view.addObject("typeMap", "contract");
        view.addObject("mapActive", "active");
        return view;
    }

    @RequestMapping("/map/station/{contract}/{number}")
    public ModelAndView map(@PathVariable String contract, @PathVariable Integer number) throws IOException {
        ModelAndView view = new ModelAndView("index");
        view.addObject("station", jcDecauxAPI.getStation(number, contract));
        view.addObject("initUrl", googleMapsAPI.getUrlInit());
        view.addObject("typeMap", "station");
        view.addObject("mapActive", "active");
        return view;
    }

    @RequestMapping("/map/contract/{name}")
    public ModelAndView map(@PathVariable String name) throws IOException {
        ModelAndView view = new ModelAndView("index");
        view.addObject("stations", jcDecauxAPI.getAllStations(name));
        view.addObject("initUrl", googleMapsAPI.getUrlInit());
        view.addObject("typeMap", "contract");
        view.addObject("mapActive", "active");
        return view;
    }

}
