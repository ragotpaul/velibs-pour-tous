package fr.eisti.controllers;

import fr.eisti.api.JCDecauxAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by ragotpaul on 1/14/17.
 */

@Controller
@Scope("session")
public class ContractController {

    @Autowired
    private JCDecauxAPI jcDecauxAPI;

    @RequestMapping("/contracts")
    public ModelAndView contracts() throws IOException {
        ModelAndView view = new ModelAndView("contracts");
        view.addObject("contracts", jcDecauxAPI.getAllContracts());
        view.addObject("contractsActive", "active");
        view.addObject("multiple", true);
        return view;
    }

    @RequestMapping("/contract/{name}")
    public ModelAndView contract(@PathVariable String name, HttpServletRequest request) throws IOException {
        ModelAndView view = new ModelAndView("contracts");
        HttpSession session = request.getSession();
        session.setAttribute("contract", jcDecauxAPI.getContract(name));
        view.addObject("contract", session.getAttribute("contract"));
        view.addObject("contractsActive", "active");
        view.addObject("multiple", false);
        return view;
    }

    @RequestMapping("/contract")
    public ModelAndView contract(HttpServletRequest request) throws IOException {
        ModelAndView view = new ModelAndView("contracts");
        HttpSession session = request.getSession();
        if (session.getAttribute("contract") == null) {
            view.addObject("contract", jcDecauxAPI.getContract("Paris")); // Ville par defaut
        } else {
            view.addObject("contract", session.getAttribute("contract"));
        }
        view.addObject("contractsActive", "active");
        view.addObject("multiple", false);
        return view;
    }

}
