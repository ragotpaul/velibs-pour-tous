package fr.eisti.controllers;

import fr.eisti.api.JCDecauxAPI;
import fr.eisti.models.Contract;
import fr.eisti.models.Login;
import fr.eisti.models.Search;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;

/*
// * Created by eisti on 14/01/17.
*/

@Controller
public class LoginController {

    @Autowired
    private JCDecauxAPI jcDecauxAPI;

    @GetMapping(path = "/loginPage")
    public ModelAndView searchForm() throws IOException {
        ModelAndView view = new ModelAndView("login");
        view.addObject("loginPage", new Login());
        return view;
    }

    @PostMapping(path = "/loginPage")
    public ModelAndView searchSubmit(@ModelAttribute Login login) throws IOException {
//        login.getUsername(); Ces deux lignes étaient là pour le debug pour voir si ça marchait :)
//        login.getPswd();
        ModelAndView view = new ModelAndView("redirect:/");
        return view;
    }

}
