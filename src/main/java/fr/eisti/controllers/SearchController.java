package fr.eisti.controllers;

import fr.eisti.api.JCDecauxAPI;
import fr.eisti.models.Contract;
import fr.eisti.models.Search;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/*
// * Created by eisti on 14/01/17.
*/

@Controller
public class SearchController {

    @Autowired
    private JCDecauxAPI jcDecauxAPI;

    @GetMapping(path = "/search")
    public ModelAndView searchForm(HttpServletRequest request) throws IOException {
        ModelAndView view = new ModelAndView("search");
        HttpSession session = request.getSession();
        view.addObject("search", new Search());
        view.addObject("contracts", jcDecauxAPI.getAllContracts());
        if (session.getAttribute("contract") == null) {
            view.addObject("stations", jcDecauxAPI.getAllStations("Paris"));
        } else {
            view.addObject("stations", jcDecauxAPI.getAllStations((Contract) session.getAttribute("contract")));
        }
        view.addObject("searchActive", "active");
        return view;
    }

    @PostMapping(path = "/search/contract")
    public ModelAndView searchSubmit(@ModelAttribute Search search) throws IOException {
        ModelAndView view = new ModelAndView("redirect:/contract/" + search.getContent());
        return view;
    }

    @PostMapping(path = "/search/station")
    public ModelAndView searchStationSubmit(@ModelAttribute Search search, HttpServletRequest request) throws IOException{
        ModelAndView view = new ModelAndView("redirect:/station/Paris/" + search.getContent());
        HttpSession session = request.getSession();
        if (session.getAttribute("contract") != null) {
            Contract contract = (Contract) session.getAttribute("contract");
            view.setViewName("redirect:/station/" + contract.getName() + "/" + search.getContent());
        }
        return view;
    }

}
