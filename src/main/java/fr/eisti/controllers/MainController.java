package fr.eisti.controllers;

import fr.eisti.api.GoogleMapsAPI;
import fr.eisti.api.JCDecauxAPI;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;

/**
 * Created by ragotpaul on 1/14/17.
 */

@Controller
public class MainController {

    protected static Logger logger = Logger.getLogger(MainController.class);

    @Autowired
    private GoogleMapsAPI googleMapsAPI;


    @RequestMapping("/")
    public ModelAndView index() throws IOException {
        ModelAndView view = new ModelAndView("index");
        view.addObject("initUrl", googleMapsAPI.getUrlInit());
        view.addObject("typeMap", "index");
        view.addObject("indexActive", "active");
        return view;
    }

}
