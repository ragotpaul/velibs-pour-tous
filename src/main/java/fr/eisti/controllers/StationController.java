package fr.eisti.controllers;

import fr.eisti.api.JCDecauxAPI;
import fr.eisti.models.Station;
import fr.eisti.models.Contract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;

/**
 * Created by ragotpaul on 1/14/17.
 */

@Controller
public class StationController {

    @Autowired
    private JCDecauxAPI jcDecauxAPI;

    @RequestMapping("/stations")
    public ModelAndView stations(HttpServletRequest request) throws IOException {
        ModelAndView view = new ModelAndView("stations");
        HttpSession session = request.getSession();
        ArrayList<Station> stations;
        if (session.getAttribute("contract") == null) {
            view.addObject("stations", jcDecauxAPI.getAllStations("Paris"));
        } else {
            view.addObject("stations", jcDecauxAPI.getAllStations((Contract) session.getAttribute("contract")));
        }
        view.addObject("stationsActive", "active");
        view.addObject("multiple", true);
        return view;
    }

    @RequestMapping("/stations/{contract}")
    public ModelAndView stations(@PathVariable String contract) throws IOException {
        ModelAndView view = new ModelAndView("stations");
        view.addObject("stations", jcDecauxAPI.getAllStations(contract));
        view.addObject("stationsActive", "active");
        view.addObject("multiple", true);
        return view;
    }

    @RequestMapping("/station/{contract}/{number}")
    public ModelAndView station(@PathVariable String contract, @PathVariable Integer number) throws IOException {
        ModelAndView view = new ModelAndView("stations");
        view.addObject("station", jcDecauxAPI.getStation(number, contract));
        view.addObject("stationsActive", "active");
        view.addObject("multiple", false);
        return view;
    }

}
