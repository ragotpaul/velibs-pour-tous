package fr.eisti.models;

import org.springframework.security.core.GrantedAuthority;

/**
 * Created by ragotpaul on 1/14/17.
 */
public enum Role implements GrantedAuthority {

    ADMIN, USER;


    @Override
    public String getAuthority() {
        return name();
    }

}
