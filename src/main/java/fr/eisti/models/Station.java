package fr.eisti.models;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by ragotpaul on 1/14/17.
 */
public class Station {

    private Integer number;

    private String name;

    private String address;

    private Position position;

    private Boolean banking;

    private Boolean bonus;

    private String status;

    private String contract_name;

    private Integer bike_stands;

    private Integer available_bike_stands;

    private Integer available_bikes;

    private Timestamp last_update;

    public Station() {
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Boolean getBanking() {
        return banking;
    }

    public void setBanking(Boolean banking) {
        this.banking = banking;
    }

    public Boolean getBonus() {
        return bonus;
    }

    public void setBonus(Boolean bonus) {
        this.bonus = bonus;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getContract_name() {
        return contract_name;
    }

    public void setContract_name(String contract_name) {
        this.contract_name = contract_name;
    }

    public Integer getBike_stands() {
        return bike_stands;
    }

    public void setBike_stands(Integer bike_stands) {
        this.bike_stands = bike_stands;
    }

    public Integer getAvailable_bike_stands() {
        return available_bike_stands;
    }

    public void setAvailable_bike_stands(Integer available_bike_stands) {
        this.available_bike_stands = available_bike_stands;
    }

    public Integer getAvailable_bikes() {
        return available_bikes;
    }

    public void setAvailable_bikes(Integer available_bikes) {
        this.available_bikes = available_bikes;
    }

    public Timestamp getLast_update() {
        return last_update;
    }

    public void setLast_update(Timestamp last_update) {
        this.last_update = last_update;
    }

    public static Station convert(LinkedHashMap origin) {
        Station result = new Station();
        result.setNumber((Integer) origin.get("number"));
        result.setName((String) origin.get("name"));
        result.setAddress((String) origin.get("address"));
        result.setPosition(Position.convert((LinkedHashMap) origin.get("position")));
        result.setBanking((Boolean) origin.get("banking"));
        result.setBonus((Boolean) origin.get("bonus"));
        result.setStatus((String) origin.get("status"));
        result.setContract_name((String) origin.get("contract_name"));
        result.setBike_stands((Integer) origin.get("bike_stands"));
        result.setAvailable_bike_stands((Integer) origin.get("available_bike_stands"));
        result.setAvailable_bikes((Integer) origin.get("available_bikes"));
        result.setLast_update(new Timestamp((Long) origin.get("last_update")));
        return result;
    }

}
