package fr.eisti.models;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Created by ragotpaul on 1/14/17.
 */
public class Position {

    private Double lat;

    private Double lng;

    public Position() {
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public static Position convert(LinkedHashMap origin) {
        Position result = new Position();
        result.setLat((Double) origin.get("lat"));
        result.setLng((Double) origin.get("lng"));
        return result;
    }

}
