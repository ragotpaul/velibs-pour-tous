package fr.eisti.models;

/**
 * Created by eisti on 14/01/17.
 */
public class Login {

    private String username;
    private String pswd;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPswd() {
        return pswd;
    }

    public void setPswd(String pswd) {
        this.pswd = pswd;
    }
}
