package fr.eisti.models;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Locale;

/**
 * Created by ragotpaul on 1/14/17.
 */

public class Contract {

    private String name;

    private String commercial_name;

    private String country_code;

    private ArrayList<String> cities;

    public Contract() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCommercial_name() {
        return commercial_name;
    }

    public void setCommercial_name(String commercial_name) {
        this.commercial_name = commercial_name;
    }

    public String getCountry_code() {
        return country_code;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

    public ArrayList<String> getCities() {
        return cities;
    }

    public void setCities(ArrayList<String> cities) {
        this.cities = cities;
    }

    public String getCountry() {
        Locale country = new Locale(country_code, country_code);
        return country.getDisplayCountry();
    }

    public static Contract convert(LinkedHashMap origin) {
        Contract result = new Contract();
        result.setName((String) origin.get("name"));
        result.setCities((ArrayList<String>) origin.get("cities"));
        result.setCommercial_name((String) origin.get("commercial_name"));
        result.setCountry_code((String) origin.get("country_code"));
        return result;
    }

}
