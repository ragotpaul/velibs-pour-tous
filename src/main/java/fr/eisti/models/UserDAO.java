package fr.eisti.models;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Created by ragotpaul on 1/14/17.
 */

// TODO: API key for our application

@RepositoryRestResource(collectionResourceRel = "user", path = "user")
public interface UserDAO extends PagingAndSortingRepository<User, Integer> {

    public User findByUsername (String username);

    public User findByEmail (String email);

}