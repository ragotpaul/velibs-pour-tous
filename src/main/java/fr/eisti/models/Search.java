package fr.eisti.models;

/**
 * Created by eisti on 14/01/17.
 */
public class Search {

    private String content;

    public Search() {
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

}
