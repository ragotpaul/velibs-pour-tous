package fr.eisti.api;

/**
 * Created by ragotpaul on 1/14/17.
 */
public abstract class AbstractAPI {

    private String url;

    private String key;

    public AbstractAPI(String url, String key) {
        this.url = url;
        this.key = key;
    }

    public AbstractAPI() {
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public abstract String getUrlInit ();

}
