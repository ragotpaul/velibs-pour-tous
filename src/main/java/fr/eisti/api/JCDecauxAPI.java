package fr.eisti.api;

import fr.eisti.models.Contract;
import fr.eisti.models.Station;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Created by ragotpaul on 1/14/17.
 */

public class JCDecauxAPI extends AbstractAPI {

    public JCDecauxAPI(String url, String key) {
        super(url, key);
    }

    public JCDecauxAPI() {
    }

    @Override
    public String getUrlInit() {
        return getUrl() + "/stations?" + getUrlEnd();
    }

    private RestTemplate template = new RestTemplate();

    private String getUrlEnd() {
        return "apiKey=" + getKey();
    }

    private ArrayList<Contract> collectContracts(ArrayList<LinkedHashMap> contracts) {
        ArrayList<Contract> result = new ArrayList<>();
        for (LinkedHashMap contract:
                contracts) {
            result.add(Contract.convert(contract));
        }
        return result;
    }

    private String getAllContractsUrl() {
        return getUrl() + "/contracts?" + getUrlEnd();
    }

    public ArrayList<Contract> getAllContracts () {
        return collectContracts(template.getForObject(getAllContractsUrl(), ArrayList.class));
    }

    public Contract getContract(String name) {
        ArrayList<Contract> contracts = getAllContracts();
        Contract result = new Contract();
        for (Contract contract :
                contracts) {
            if (contract.getName().equals(name)) {
                result = contract;
            }
        }
        return result;
    }

    private String getStationUrl (Integer number, String contract) {
        return getUrl() + "/stations/" + number + "?contract=" + contract + "&" + getUrlEnd();
    }

    public Station getStation(Integer number, String contract) {
        return template.getForObject(getStationUrl(number, contract), Station.class);
    }

    private ArrayList<Station> collectStations(ArrayList<LinkedHashMap> stations) {
        ArrayList<Station> result = new ArrayList<>();
        for (LinkedHashMap station:
                stations) {
            result.add(Station.convert(station));
        }
        return result;
    }

    private String getAllStationUrl () {
        return getUrlInit();
    }

    private String getAllStationUrl (String contract) {
        return getUrl() + "/stations?contract=" + contract + "&" + getUrlEnd();
    }

    public ArrayList<Station> getAllStations() {
        return collectStations(template.getForObject(getAllStationUrl(), ArrayList.class));
    }

    public ArrayList<Station> getAllStations(String contract) {
        return collectStations(template.getForObject(getAllStationUrl(contract), ArrayList.class));
    }

    public ArrayList<Station> getAllStations(Contract contract) {
        return collectStations(template.getForObject(getAllStationUrl(contract.getName()), ArrayList.class));
    }



}
