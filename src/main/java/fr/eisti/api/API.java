package fr.eisti.api;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * Created by ragotpaul on 1/14/17.
 */

@Configuration
@PropertySource("classpath:api.properties")
public class API {

    @Value("${google.maps.api.key}")
    private String googleMapsApiKey;

    @Value("${google.maps.api.base_url}")
    private String googleMapsApiUrl;

    @Bean
    public GoogleMapsAPI googleMapsAPI() {
        return new GoogleMapsAPI(googleMapsApiUrl, googleMapsApiKey);
    }

    @Value("${jcdecaux.api.key}")
    private String jcDecauxApiKey;

    @Value("${jcdecaux.api.base_url}")
    private String jcDecauxApiUrl;

    @Bean
    public JCDecauxAPI jcDecauxAPI() {
        return new JCDecauxAPI(jcDecauxApiUrl, jcDecauxApiKey);
    }

}
