package fr.eisti.api;

/**
 * Created by ragotpaul on 1/14/17.
 */

public class GoogleMapsAPI extends AbstractAPI {

    public GoogleMapsAPI(String url, String key) {
        super(url, key);
    }

    public GoogleMapsAPI() {
    }

    @Override
    public String getUrlInit() {
        return getUrl() + "?key=" + getKey() + "&callback=initMap";
    }

}
